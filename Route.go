package slApi

type Route struct {
	Method 		string
	Path 		string
	Handler 	func(RequestInterface) ResponseInterface
}
