package slApi

type RouterInterface interface {
	 LoadRoute(Route)
	 Start()
}