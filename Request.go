package slApi

import "io"

type RequestInterface interface {
	GetUrlParam(string) 	string
	GetBody()				io.ReadCloser
}

type RequestObject struct {
	urlParameters 			map[string]string
	body 					io.ReadCloser
}


func NewRequest() *RequestObject {
	req := &RequestObject{}
	req.urlParameters = make(map[string]string)

	return req
}

func (request *RequestObject) SetUrlParam(name string, value string) {
	if request.urlParameters == nil {
		request.urlParameters = make(map[string]string)
	}

	request.urlParameters[name] = value
}

func (request *RequestObject) GetUrlParam(name string) string {
	if request.urlParameters == nil {
		return ""
	}

	return request.urlParameters[name]
}

func (request *RequestObject) SetBody(b io.ReadCloser) {
	request.body = b
}

func (request *RequestObject) GetBody() io.ReadCloser {
	return request.body
}
