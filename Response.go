package slApi

type ResponseInterface interface {
	SetBody(string)
	GetBody() string
}

type Response struct {
	Body string
}

func (r *Response) SetBody(b string) {
	r.Body = b
}

func (r *Response) GetBody() string {
	return r.Body
}

func NewResponse() ResponseInterface {
	return &Response{}
}
